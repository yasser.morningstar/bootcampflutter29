void main() {
  //NO 1
  // var deret = 0;
  // print("LOOPING PERTAMA");
  // while (deret < 20) {
  //   deret += 2;
  //   print(deret.toString() + ' - I love coding ');
  // }
  // print("LOOPING KEDUA");
  // var deret2 = 22;
  // while (deret2 > 2) {
  //   deret2 -= 2;
  //   print(deret2.toString() + ' - I will become a mobile developer');
  // }

  //NO 2
  // for (var angka = 1; angka < 21; angka++) {
  //   if (angka % 2 == 0) {
  //     print(angka.toString() + " - Berkualitas");
  //   } else if (angka % 3 == 0) {
  //     print(angka.toString() + " - I Love Coding.");
  //   } else {
  //     print(angka.toString() + " - Santai");
  //   }
  // }

  //NO 3
  // for (var angka = 1; angka <= 4; angka++) {
  //   print("########");
  // }

  //NO 4
  // int i = 1;
  // int j = 1;
  // var a = 7;
  // var t = 7;
  // var sym = "";
  // for (i = 1; i <= t; i++) {
  //   for (j = 1; j <= i; j++) {
  //     sym += "#";
  //   }
  //   print(sym);
  //   sym = "#";
  // }
}
