import 'dart:io';

void main() {
//No 1
  // print("(y/t) : ");
  // var answer = stdin.readLineSync();
  // if (answer == "Y") {
  //   print("anda akan menginstall aplikasi dart");
  // } else {
  //   print("aborted");
  // }

//No 2

  // print("Input Nama  = ");
  // var nama = stdin.readLineSync();
  // print("Input Peran  = ");
  // var peran = stdin.readLineSync();
  // if (nama == "" || peran == "") {
  //   print("apabila kosong semua Nama harus diisi!");
  // } else if (peran == "") {
  //   print("Halo $nama, Pilih peranmu untuk memulai game!");
  // } else if (peran == "Penyihir") {
  //   print("Selamat datang di Dunia Werewolf, $nama "
  //       " Halo Penyihir $nama, kamu dapat melihat siapa yang menjadi werewolf!");
  // } else if (peran == "Guard") {
  //   print("Selamat datang di Dunia Werewolf, $nama "
  //       "Halo Guard $nama, kamu akan membantu melindungi temanmu dari serangan werewolf.");
  // } else if (peran == "Warewolf") {
  //   print("Selamat datang di Dunia Werewolf, $nama "
  //       "Halo Werewolf $nama, Kamu akan memakan mangsa setiap malam!");
  // } else {
  //   print("Peran/Nama Tidak ada!");
  // }

  //No 3
  // print("Quote Pada Hari : ");
  // var hari = stdin.readLineSync();
  // switch (hari) {
  //   case "Senin":
  //     {
  //       print(
  //           'Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.');
  //       break;
  //     }
  //   case "Selasa":
  //     {
  //       print(
  //           'Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.');
  //       break;
  //     }
  //   case "Rabu":
  //     {
  //       print(
  //           'Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.');
  //       break;
  //     }
  //   case "Kamis":
  //     {
  //       print(
  //           'Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.');
  //       break;
  //     }
  //   case "Jumat":
  //     {
  //       print('Hidup tak selamanya tentang pacar.');
  //       break;
  //     }
  //   case "Sabtu":
  //     {
  //       print('Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.');
  //       break;
  //     }
  //   case "Minggu":
  //     {
  //       print(
  //           'Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.');
  //       break;
  //     }
  //   default:
  //     {
  //       print('Hari tidak ditemukan');
  //     }
  // }

  //No 4
  // var tanggal = 10;
  // var bulan = 1;
  // var tahun = 1945;
  // switch (bulan) {
  //   case 1:
  //     {
  //       print('$tanggal Januari $tahun');
  //       break;
  //     }
  //   case 2:
  //     {
  //       print('$tanggal Febuari $tahun');
  //       break;
  //     }
  //   case 3:
  //     {
  //       print('$tanggal Maret $tahun');
  //       break;
  //     }
  //   case 4:
  //     {
  //       print('$tanggal April $tahun');
  //       break;
  //     }
  //   case 5:
  //     {
  //       print('$tanggal Mei $tahun');
  //       break;
  //     }
  //   case 6:
  //     {
  //       print('$tanggal Juni $tahun');
  //       break;
  //     }
  //   case 7:
  //     {
  //       print('$tanggal Juli $tahun');
  //       break;
  //     }
  //   case 8:
  //     {
  //       print('$tanggal Agustus $tahun');
  //       break;
  //     }
  //   case 9:
  //     {
  //       print('$tanggal September $tahun');
  //       break;
  //     }
  //   case 10:
  //     {
  //       print('$tanggal Oktober $tahun');
  //       break;
  //     }
  //   case 11:
  //     {
  //       print('$tanggal November $tahun');
  //       break;
  //     }
  //   case 12:
  //     {
  //       print('$tanggal Desember $tahun');
  //       break;
  //     }
  //   default:
  //     {
  //       print('Bulan Tidak di temukan');
  //     }
  // }
}
